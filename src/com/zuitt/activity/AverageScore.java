package com.zuitt.activity;

import java.util.Scanner;

public class AverageScore {

    public static void main(String[] args){

        String firstName;
        String lastName;
        Double firstSubject;
        Double secondSubject;
        Double thirdSubject;

        Scanner userInput = new Scanner(System.in);

        System.out.print("First Name: ");
        firstName = userInput.nextLine();

        System.out.print("Last Name: ");
        lastName = userInput.nextLine();

        String myName = firstName + " " + lastName;

        System.out.print("First Subject Grade: ");
        firstSubject = userInput.nextDouble();

        System.out.print("Second Subject Grade: ");
        secondSubject = userInput.nextDouble();

        System.out.print("Third Subject Grade: ");
        thirdSubject = userInput.nextDouble();

        Double gradeAve = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good Day, " + myName + ".");
        System.out.println("Your grade average is: " + gradeAve);
    }
}
